<?php

use Catalog\Enums\CatalogEnums;
use Laravel\Fortify\Features;
use Quest\Enums\MenuEnums;

return [
    'title' => 'Вопросы',
    'fillable' => [
        'quest' => [
            'title','quest','order'
        ],
    ],
    'request' => [
        'zone' => [
            'title'=> ['required','string'],
            'cords'=> ['required','json'],
            'color'=> ['required','string'],
            'width'=> ['required','integer'],
            'fill'=> ['required','string'],
            'description'=> ['nullable','string']
        ],
        'category' => [
            'title' => ['required','string'],
        ],
        'icon' => [
            'url' => ['required','string'],
            'icon' => ['required', 'file', 'dimensions:min_width=27,min_height=32,max_width=27,max_height=32'],
        ],
        'type' => [
            'title' => ['required','string'],
            'icon' => ['required'],
        ],
        'marker' => [
            'cord' => ['required','json'],
            'title' => ['nullable','string'],
            'description' => ['nullable','string'],
            'type_id' => ['nullable'],
            'category_id' => ['nullable'],
            'address' => ['required','string'],
        ],
        'settings' => [
            'api_key' => ['required','string'],
            'lang' => ['required'],
            'seo_title' => ['nullable'],
            'seo_url' => ['nullable'],
            'seo_description' => ['nullable']
        ]
    ],
];
