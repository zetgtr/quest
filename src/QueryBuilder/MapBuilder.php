<?php

namespace Map\QueryBuilder;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use PhpParser\Node\Expr\Cast\Object_;

class MapBuilder
{
    public function getNavigationLinks($key = null)
    {
        $links = config('admin.menu_links');
        return $this->setLink($links,$key);
    }

    public function getNavigationLinksSettings($key = null)
    {
        $links = config('admin.menu_settings');
        return $this->setLink($links,$key);
    }
    public function getNavigationLinksType($key = null)
    {
        $links = config('admin.menu_type');
        return $this->setLink($links,$key);
    }

    private function setLink($links,$key){
        if($key)
            $links[$key]['active'] = true;
        return $links;
    }
}
