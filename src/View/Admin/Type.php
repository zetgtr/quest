<?php

namespace Map\View\Admin;

use Illuminate\View\Component;
use Map\Models\MapIcon;
use Map\Models\MapSettings;
use Map\Models\MapType;

class Type extends Component
{

    public function __construct()
    {
        $this->icons = MapIcon::get();
        $this->types = MapType::get();
    }


    public function render()
    {
        return view('admin::components.type.type',['icons'=>$this->icons,'types' => $this->types]);
    }
}
