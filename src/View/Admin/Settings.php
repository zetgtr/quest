<?php

namespace Map\View\Admin;

use Illuminate\View\Component;
use Map\Models\MapSettings;

class Settings extends Component
{

    public function __construct()
    {
        $this->settings = MapSettings::first();
    }


    public function render()
    {
        return view('admin::components.settings.index',['settings'=>$this->settings]);
    }
}
