<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TypeCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return config('admin.request.type');
    }

    public function authorize(): bool
    {
        return true;
    }
}
