export class Map {
    constructor() {
        this.initMap();
    }

    async initMap() {
        await ymaps3.ready;
        const {YMap, YMapDefaultSchemeLayer,YMapDefaultFeaturesLayer} = ymaps3;
        this.map = new YMap(
            document.getElementById('map'),
            {location: {
                    center: [30.31468,59.939452],
                    zoom: 10
                }, showScaleInCopyrights: true},
            [
                new YMapDefaultSchemeLayer({}),
                new YMapDefaultFeaturesLayer({})
            ]
        );

        this.map.addChild(new YMapDefaultSchemeLayer());
    }
}
