class TypeAddress {
    constructor(template,el,addPosition) {
        this.template = template
        this.addPosition = addPosition
        this.el = el
        this.typeSelect = this.template.querySelector('#types')
        this.addEvent()
    }

    addEvent(){
        this.typeSelect.addEventListener('input',this.setType.bind(this))
    }

    setType(){
        if (!this.el){
            this.el = document.querySelector('.delete_marker')
        }
        if(this.el){
            this.el.innerHTML = ''
            let markerElement
            if (this.typeSelect.selectedOptions[0].dataset.img) {
                markerElement = document.createElement('div')
                const img = document.createElement('img')
                img.src = this.typeSelect.selectedOptions[0].dataset.img
                markerElement.classList.add('marker')
                markerElement.append(img)
            }
            else {
                markerElement = document.createElement('i');
                markerElement.className = 'fas fa-admin-marker-alt marker_default';
            }
            this.addPosition.type_id = this.typeSelect.value
            console.log(this.el)
            this.el.append(markerElement)
        }
    }
}

export default TypeAddress
