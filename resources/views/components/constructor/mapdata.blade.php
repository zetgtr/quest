<div class="map__data container_map_data">
    @if ($categories)
        @foreach ($categories as $category)
            <div class="data__category map_data_category_{{ $category->id }} map_data_category_item">
                <h3 class="data__title map_data__title">{{ $category->title }}</h3>
                <div class="data__items container_map_data_item">
                    @foreach ($category->markers as $marker)
                        <div data-cord="{{ $marker->cord }}"
                             class="item marker_data_item_{{ $marker->id }} map_data_item">
                            <h4 class="item__title">{{ $marker->title }}</h4>
                            <p class="item__desc">{{ $marker->description }}</p>
                            <p class="item__address">{{ $marker->address }}</p>
                            @if ($marker->type)
                                <div class="item__marker d-flex">
                                    <div class="marker__icon">
                                        <img src="{{ $marker->type->icons->url }}">
                                    </div>
                                    <div class="marker__title">
                                        <p>
                                            {{ $marker->type->title }}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    @endif
</div>
