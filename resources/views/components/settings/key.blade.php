<div class="tab-pane active fide" id="{{ \Map\Enums\MenuEnums::KEY->value }}" role="tabpanel">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Ключ API</label>
                <input type="text" class="form-control @error('api_key') is_invalid @enderror" name="api_key"
                       value="{{ old('api_key',$settings ? $settings->api_key : "") }}">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Локализация API</label>
                <select name="lang" class="form-control form-select @error('lang') is_invalid @enderror">
                    <option @selected(old('lang', $settings ? $settings->lang : "") == "ru_RU") value="ru_RU">ru_RU
                    </option>
                    <option @selected(old('lang', $settings ? $settings->lang : "") == "en_US") value="en_US">en_US
                    </option>
                    <option @selected(old('lang', $settings ? $settings->lang : "") == "en_RU") value="en_RU">en_RU
                    </option>
                </select>
            </div>
        </div>
    </div>
</div>
