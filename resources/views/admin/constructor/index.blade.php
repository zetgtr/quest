@extends('layouts.admin')
@section('title',config('admin.title'))
@section('content')
    @vite('resources/assets/scss/admin/admin-admin.scss')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <div class="card-body d-flex">

            <div style="width: 0px;transition: width 0.5s ease;" class="container_info"></div>
            <div style="width: 100%;transition: width 0.5s ease;" class="container_map">
                <input type="hidden" id="data_second">
                <input type="hidden" id="markers" value="{{ $markers }}">
                <input type="hidden" id="zone_data" value="{{ $zone_data }}">
                <div  id="map" style="width: 100%; height: 600px"></div>
            </div>
        </div>
    </div>

    <script src="https://api-maps.yandex.ru/v3/?apikey={{ $settings->api_key }}&lang={{ $settings->lang }}"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang={{ $settings->lang }}&amp;apikey={{ $settings->api_key }}" type="text/javascript"></script>
    @vite('resources/js/admin/admin/index.js')
    <x-map::constructor.add-position :categories="$categories" :types="$types" />
    <x-map::constructor.category :categories="$categories" />
    <x-map::constructor.type :types="$types" />
    <x-map::constructor.marker  />
    <x-map::constructor.zone  />
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ config('admin.title') }}</li>
        </ol>
    </div>
@endsection
