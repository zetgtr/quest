@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <div class="card-body">
            <x-warning />
            <div class="row row-page-create">
                <div class="col-md-8 order-md-first">
                    <div class="dd nestable" id="nestable">
                        <x-map::category.dnd :categories="$categories"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <form action="{{ route('admin.admin.category.store') }}" method="POST">
                        @csrf
                        <h4 class="info_title">Добавить категорию</h4>
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                        <input type="submit" value="Добавить" class="btn btn-success btn-sm">
                    </form>


                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="route_dd" value="{{  route('admin.admin.category.order') }}">
    <script src="{{asset('assets/js/admin/dnd.js')}}" ></script>
    <script src="{{asset('assets/js/admin/delete.js')}}" ></script>
    <script src="{{ asset('assets/js/admin/show.js') }}"></script>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.admin")}}">{{ config('admin.title') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список категорий</li>
        </ol>
    </div>
@endsection
