<?php

use Catalog\Http\Controllers\CartController;
use Catalog\Http\Controllers\CatalogCategoryController;
use Catalog\Http\Controllers\CatalogProductController;
use Illuminate\Support\Facades\Route;


//
//Route::middleware('api')->group(function (){
//    Route::group(['prefix'=>"api"],static function(){
//        Route::group(['prefix' => 'admin', 'as' => 'admin.'], static function() {
//            Route::get('catalog_search/{text}',[CatalogProductController::class,'search']);
//        });
//        Route::post('/get_filter',[CatalogProductController::class,'filter']);
//        Route::post('/get_filter_view',[CatalogProductController::class,'filterView']);
//        Route::get('catalog_route', [CatalogCategoryController::class, 'getCatalogRouter']);
//        Route::get('get_catalog/{url}', [CatalogCategoryController::class, 'getCatalog']);
//        Route::get('get_catalog_breadcrumb/{url}', [CatalogCategoryController::class, 'getBreadcrumb']);
//
//    });
//});
