<?php


use Illuminate\Support\Facades\Route;



Route::middleware('web')->group(function (){
   Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => ['menu.check:978','is_admin']],static function() {
       Route::get('quest', [QuestController::class, 'index'])->name('quest');
   });
});

