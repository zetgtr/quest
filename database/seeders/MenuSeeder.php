<?php

namespace Quest\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>978,'name'=>config('quest.title'),'position'=>'left','logo'=>'fal fa-question','controller'=>'Quest\Http\Controllers\QuestController','url'=>'admin','parent'=>5,"controller_type"=>"invocable", 'order'=>4],
        ];
    }
}
